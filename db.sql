CREATE DATABASE QLSV;
CREATE TABLE DMKHOA (
	MaKH varchar(6) NOT NULL,
    TenKhoa varchar(30),
    PRIMARY KEY (MaKH)
);
CREATE TABLE SINHVIEN (
	MaSV varchar(6) NOT NULL,
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh DateTime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int,
    PRIMARY KEY (MaSV)
);